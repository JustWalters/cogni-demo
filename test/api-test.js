'use strict';

const mongoose = require('mongoose');
const assert = require('assert');
const { Transaction } = require('../transaction');
const { getTransactions } = require('../api');

describe('API', function() {
	before(function(done) {
		mongoose.connect('mongodb://localhost/cogni-test', done);
	});

	after(function() {
		mongoose.connection.close();
	});

	describe('getTransactions', function() {
		let ids = [];

		before(function(done) {
			let transactions = [{
				location: {
					address: '50 W 34th St',
					city: 'New York',
					state: 'NY',
					zip: 10001
				},
				accountId: '30',
				category: 'Shops_Flea Markets',
				amount: 20,
				date: new Date(2005, 11, 15, 9, 54)
			}, {
				location: {
					address: '70 E 106th St',
					city: 'New York',
					state: 'NY',
					zip: 10031
				},
				accountId: '54',
				category: 'Recreation_Arts and Entertainment_Movie Theatres',
				amount: 300,
				date: new Date(2017, 10, 5, 10, 25)
			}];
			Transaction.create(transactions)
				.then(function(docs) {
					ids = docs.map(doc => doc._id);
					done();
				})
				.catch(done);
		});

		after(function(done) {
			Transaction.remove({}, done);
		});

		it('should be able to query by transaction amount', async function() {
			let query = { minAmount: '300' };
			let res = await getTransactions(query);
			let transactions = res[0].transactions;
			assert.equal(transactions.length, 1);
			assert(ids[1].equals(transactions[0]._id));

			query = { minAmount: '0', maxAmount: '300' };
			res = await getTransactions(query);
			transactions = res[0].transactions;
			assert.equal(transactions.length, 2);
		});

		it('should be able to switch minimum and maximum amount if necessary', async function() {
			let query = { minAmount: '305', maxAmount: '295' };
			let res = await getTransactions(query);
			let transactions = res[0].transactions;
			assert.equal(transactions.length, 1);
			assert(ids[1].equals(transactions[0]._id));
		});

		it('should be able to query by transaction category', async function() {
			let query = { category: 'Shops_Flea Markets' };
			let res = await getTransactions(query);
			let transactions = res[0].transactions;
			assert.equal(transactions.length, 1);
			assert(ids[0].equals(transactions[0]._id));
		});

		it('should be able to query with multiple categories', async function() {
			let query = { category: ['Shops_Flea Markets', 'Recreation_Arts and Entertainment_Movie Theatres'] };
			let res = await getTransactions(query);
			let transactions = res[0].transactions;
			assert.equal(transactions.length, 2)
		});

		it('should be able to query by transaction parent category', async function() {
			let query = { category: 'Shops' };
			let res = await getTransactions(query);
			let transactions = res[0].transactions;
			assert.equal(transactions.length, 1);
			assert(ids[0].equals(transactions[0]._id));
		});

		it('should be able to query by transaction day', async function() {
			let query = { day: 15, month: 12, year: 2005 };
			let res = await getTransactions(query);
			let transactions = res[0].transactions;
			assert.equal(transactions.length, 1);
			assert(ids[0].equals(transactions[0]._id));
		});

		it('should be able to query by transaction month', async function() {
			// Search for November transactions
			let query = { month: '11' };
			let res = await getTransactions(query);
			let transactions = res[0].transactions;
			assert.equal(transactions.length, 1);
			assert(ids[1].equals(transactions[0]._id));
		});

		it('should be able to query by transaction year', async function() {
			let query = { year: '2005' };
			let res = await getTransactions(query);
			let transactions = res[0].transactions;
			assert.equal(transactions.length, 1);
			assert(ids[0].equals(transactions[0]._id));
		});

		it('should be able to query by transaction month and year', async function() {
			let query = { month: '12', year: '2005' };
			let res = await getTransactions(query);
			let transactions = res[0].transactions;
			assert.equal(transactions.length, 1);
			assert(ids[0].equals(transactions[0]._id));

			query.year = 2006;
			res = await getTransactions(query);
			transactions = res[0].transactions;
			assert.equal(transactions.length, 0);
		});

		it('should be able to query by day of the week', async function() {
			// Search for Thursday transactions
			let query = { dayOfWeek: '5' };
			let res = await getTransactions(query);
			let transactions = res[0].transactions;
			assert.equal(transactions.length, 1);
			assert(ids[0].equals(transactions[0]._id));
		});

		it('should be able to query with multiple days of the week', async function() {
			let query = { dayOfWeek: ['1', '5'] };
			let res = await getTransactions(query);
			let transactions = res[0].transactions;
			assert.equal(transactions.length, 2)
		});

		it('should accept query params as numbers', async function() {
			let query = { year: 2005, amount: 20 };
			let res = await getTransactions(query);
			let transactions = res[0].transactions;
			assert.equal(transactions.length, 1);
			assert(ids[0].equals(transactions[0]._id));
		});
	});
});
