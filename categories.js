'use strict';

// Using a flattened version of Plaid's categories, from https://plaid.com/docs/api/#categories

const categories = [
	"Bank Fees",
	"Bank Fees_Overdraft",
	"Bank Fees_ATM",
	"Bank Fees_Late Payment",
	"Bank Fees_Fraud Dispute",
	"Bank Fees_Foreign Transaction",
	"Bank Fees_Wire Transfer",
	"Bank Fees_Insufficient Funds",
	"Bank Fees_Cash Advance",
	"Bank Fees_Excess Activity",
	"Cash Advance",
	"Community",
	"Community_Animal Shelter",
	"Community_Assisted Living Services",
	"Community_Assisted Living Services_Facilities and Nursing Homes",
	"Community_Assisted Living Services_Caretakers",
	"Community_Cemetery",
	"Community_Courts",
	"Community_Day Care and Preschools",
	"Community_Disabled Persons Services",
	"Community_Drug and Alcohol Services",
	"Community_Education",
	"Community_Education_Vocational Schools",
	"Community_Education_Tutoring and Educational Services",
	"Community_Education_Primary and Secondary Schools",
	"Community_Education_Fraternities and Sororities",
	"Community_Education_Driving Schools",
	"Community_Education_Dance Schools",
	"Community_Education_Culinary Lessons and Schools",
	"Community_Education_Computer Training",
	"Community_Education_Colleges and Universities",
	"Community_Education_Art School",
	"Community_Education_Adult Education",
	"Community_Government Departments and Agencies",
	"Community_Government Lobbyists",
	"Community_Housing Assistance and Shelters",
	"Community_Law Enforcement",
	"Community_Law Enforcement_Police Stations",
	"Community_Law Enforcement_Fire Stations",
	"Community_Law Enforcement_Correctional Institutions",
	"Community_Libraries",
	"Community_Military",
	"Community_Organizations and Associations",
	"Community_Organizations and Associations_Youth Organizations",
	"Community_Organizations and Associations_Environmental",
	"Community_Organizations and Associations_Charities and Non-Profits",
	"Community_Post Offices",
	"Community_Public and Social Services",
	"Community_Religious",
	"Community_Religious_Temple",
	"Community_Religious_Synagogues",
	"Community_Religious_Mosques",
	"Community_Religious_Churches",
	"Community_Senior Citizen Services",
	"Community_Senior Citizen Services_Retirement",
	"Food and Drink",
	"Food and Drink_Bar",
	"Food and Drink_Bar_Wine Bar",
	"Food and Drink_Bar_Sports Bar",
	"Food and Drink_Bar_Hotel Lounge",
	"Food and Drink_Breweries",
	"Food and Drink_Internet Cafes",
	"Food and Drink_Nightlife",
	"Food and Drink_Nightlife_Strip Club",
	"Food and Drink_Nightlife_Night Clubs",
	"Food and Drink_Nightlife_Karaoke",
	"Food and Drink_Nightlife_Jazz and Blues Cafe",
	"Food and Drink_Nightlife_Hookah Lounges",
	"Food and Drink_Nightlife_Adult Entertainment",
	"Food and Drink_Restaurants",
	"Food and Drink_Restaurants_Winery",
	"Food and Drink_Restaurants_Vegan and Vegetarian",
	"Food and Drink_Restaurants_Turkish",
	"Food and Drink_Restaurants_Thai",
	"Food and Drink_Restaurants_Swiss",
	"Food and Drink_Restaurants_Sushi",
	"Food and Drink_Restaurants_Steakhouses",
	"Food and Drink_Restaurants_Spanish",
	"Food and Drink_Restaurants_Seafood",
	"Food and Drink_Restaurants_Scandinavian",
	"Food and Drink_Restaurants_Portuguese",
	"Food and Drink_Restaurants_Pizza",
	"Food and Drink_Restaurants_Moroccan",
	"Food and Drink_Restaurants_Middle Eastern",
	"Food and Drink_Restaurants_Mexican",
	"Food and Drink_Restaurants_Mediterranean",
	"Food and Drink_Restaurants_Latin American",
	"Food and Drink_Restaurants_Korean",
	"Food and Drink_Restaurants_Juice Bar",
	"Food and Drink_Restaurants_Japanese",
	"Food and Drink_Restaurants_Italian",
	"Food and Drink_Restaurants_Indonesian",
	"Food and Drink_Restaurants_Indian",
	"Food and Drink_Restaurants_Ice Cream",
	"Food and Drink_Restaurants_Greek",
	"Food and Drink_Restaurants_German",
	"Food and Drink_Restaurants_Gastropub",
	"Food and Drink_Restaurants_French",
	"Food and Drink_Restaurants_Food Truck",
	"Food and Drink_Restaurants_Fish and Chips",
	"Food and Drink_Restaurants_Filipino",
	"Food and Drink_Restaurants_Fast Food",
	"Food and Drink_Restaurants_Falafel",
	"Food and Drink_Restaurants_Ethiopian",
	"Food and Drink_Restaurants_Eastern European",
	"Food and Drink_Restaurants_Donuts",
	"Food and Drink_Restaurants_Distillery",
	"Food and Drink_Restaurants_Diners",
	"Food and Drink_Restaurants_Dessert",
	"Food and Drink_Restaurants_Delis",
	"Food and Drink_Restaurants_Cupcake Shop",
	"Food and Drink_Restaurants_Cuban",
	"Food and Drink_Restaurants_Coffee Shop",
	"Food and Drink_Restaurants_Chinese",
	"Food and Drink_Restaurants_Caribbean",
	"Food and Drink_Restaurants_Cajun",
	"Food and Drink_Restaurants_Cafe",
	"Food and Drink_Restaurants_Burrito",
	"Food and Drink_Restaurants_Burgers",
	"Food and Drink_Restaurants_Breakfast Spot",
	"Food and Drink_Restaurants_Brazilian",
	"Food and Drink_Restaurants_Barbecue",
	"Food and Drink_Restaurants_Bakery",
	"Food and Drink_Restaurants_Bagel Shop",
	"Food and Drink_Restaurants_Australian",
	"Food and Drink_Restaurants_Asian",
	"Food and Drink_Restaurants_American",
	"Food and Drink_Restaurants_African",
	"Food and Drink_Restaurants_Afghan",
	"Healthcare",
	"Healthcare_Healthcare Services",
	"Healthcare_Healthcare Services_Psychologists",
	"Healthcare_Healthcare Services_Pregnancy and Sexual Health",
	"Healthcare_Healthcare Services_Podiatrists",
	"Healthcare_Healthcare Services_Physical Therapy",
	"Healthcare_Healthcare Services_Optometrists",
	"Healthcare_Healthcare Services_Nutritionists",
	"Healthcare_Healthcare Services_Nurses",
	"Healthcare_Healthcare Services_Mental Health",
	"Healthcare_Healthcare Services_Medical Supplies and Labs",
	"Healthcare_Healthcare Services_Hospitals, Clinics and Medical Centers",
	"Healthcare_Healthcare Services_Emergency Services",
	"Healthcare_Healthcare Services_Dentists",
	"Healthcare_Healthcare Services_Counseling and Therapy",
	"Healthcare_Healthcare Services_Chiropractors",
	"Healthcare_Healthcare Services_Blood Banks and Centers",
	"Healthcare_Healthcare Services_Alternative Medicine",
	"Healthcare_Healthcare Services_Acupuncture",
	"Healthcare_Physicians",
	"Healthcare_Physicians_Urologists",
	"Healthcare_Physicians_Respiratory",
	"Healthcare_Physicians_Radiologists",
	"Healthcare_Physicians_Psychiatrists",
	"Healthcare_Physicians_Plastic Surgeons",
	"Healthcare_Physicians_Pediatricians",
	"Healthcare_Physicians_Pathologists",
	"Healthcare_Physicians_Orthopedic Surgeons",
	"Healthcare_Physicians_Ophthalmologists",
	"Healthcare_Physicians_Oncologists",
	"Healthcare_Physicians_Obstetricians and Gynecologists",
	"Healthcare_Physicians_Neurologists",
	"Healthcare_Physicians_Internal Medicine",
	"Healthcare_Physicians_General Surgery",
	"Healthcare_Physicians_Gastroenterologists",
	"Healthcare_Physicians_Family Medicine",
	"Healthcare_Physicians_Ear, Nose and Throat",
	"Healthcare_Physicians_Dermatologists",
	"Healthcare_Physicians_Cardiologists",
	"Healthcare_Physicians_Anesthesiologists",
	"Interest",
	"Interest_Interest Earned",
	"Interest_Interest Charged",
	"Payment",
	"Payment_Credit Card",
	"Payment_Rent",
	"Payment_Loan",
	"Recreation",
	"Recreation_Arts and Entertainment",
	"Recreation_Arts and Entertainment_Theatrical Productions",
	"Recreation_Arts and Entertainment_Symphony and Opera",
	"Recreation_Arts and Entertainment_Sports Venues",
	"Recreation_Arts and Entertainment_Social Clubs",
	"Recreation_Arts and Entertainment_Psychics and Astrologers",
	"Recreation_Arts and Entertainment_Party Centers",
	"Recreation_Arts and Entertainment_Music and Show Venues",
	"Recreation_Arts and Entertainment_Museums",
	"Recreation_Arts and Entertainment_Movie Theatres",
	"Recreation_Arts and Entertainment_Fairgrounds and Rodeos",
	"Recreation_Arts and Entertainment_Entertainment",
	"Recreation_Arts and Entertainment_Dance Halls and Saloons",
	"Recreation_Arts and Entertainment_Circuses and Carnivals",
	"Recreation_Arts and Entertainment_Casinos and Gaming",
	"Recreation_Arts and Entertainment_Bowling",
	"Recreation_Arts and Entertainment_Billiards and Pool",
	"Recreation_Arts and Entertainment_Art Dealers and Galleries",
	"Recreation_Arts and Entertainment_Arcades and Amusement Parks",
	"Recreation_Arts and Entertainment_Aquarium",
	"Recreation_Athletic Fields",
	"Recreation_Baseball",
	"Recreation_Basketball",
	"Recreation_Batting Cages",
	"Recreation_Boating",
	"Recreation_Campgrounds and RV Parks",
	"Recreation_Canoes and Kayaks",
	"Recreation_Combat Sports",
	"Recreation_Cycling",
	"Recreation_Dance",
	"Recreation_Equestrian",
	"Recreation_Football",
	"Recreation_Go Carts",
	"Recreation_Golf",
	"Recreation_Gun Ranges",
	"Recreation_Gymnastics",
	"Recreation_Gyms and Fitness Centers",
	"Recreation_Hiking",
	"Recreation_Hockey",
	"Recreation_Hot Air Balloons",
	"Recreation_Hunting and Fishing",
	"Recreation_Landmarks",
	"Recreation_Landmarks_Monuments and Memorials",
	"Recreation_Landmarks_Historic Sites",
	"Recreation_Landmarks_Gardens",
	"Recreation_Landmarks_Buildings and Structures",
	"Recreation_Miniature Golf",
	"Recreation_Outdoors",
	"Recreation_Outdoors_Rivers",
	"Recreation_Outdoors_Mountains",
	"Recreation_Outdoors_Lakes",
	"Recreation_Outdoors_Forests",
	"Recreation_Outdoors_Beaches",
	"Recreation_Paintball",
	"Recreation_Parks",
	"Recreation_Parks_Playgrounds",
	"Recreation_Parks_Picnic Areas",
	"Recreation_Parks_Natural Parks",
	"Recreation_Personal Trainers",
	"Recreation_Race Tracks",
	"Recreation_Racquet Sports",
	"Recreation_Racquetball",
	"Recreation_Rafting",
	"Recreation_Recreation Centers",
	"Recreation_Rock Climbing",
	"Recreation_Running",
	"Recreation_Scuba Diving",
	"Recreation_Skating",
	"Recreation_Skydiving",
	"Recreation_Snow Sports",
	"Recreation_Soccer",
	"Recreation_Sports and Recreation Camps",
	"Recreation_Sports Clubs",
	"Recreation_Stadiums and Arenas",
	"Recreation_Swimming",
	"Recreation_Tennis",
	"Recreation_Water Sports",
	"Recreation_Yoga and Pilates",
	"Recreation_Zoo",
	"Service",
	"Service_Advertising and Marketing",
	"Service_Advertising and Marketing_Writing, Copywriting and Technical Writing",
	"Service_Advertising and Marketing_Search Engine Marketing and Optimization",
	"Service_Advertising and Marketing_Public Relations",
	"Service_Advertising and Marketing_Promotional Items",
	"Service_Advertising and Marketing_Print, TV, Radio and Outdoor Advertising",
	"Service_Advertising and Marketing_Online Advertising",
	"Service_Advertising and Marketing_Market Research and Consulting",
	"Service_Advertising and Marketing_Direct Mail and Email Marketing Services",
	"Service_Advertising and Marketing_Creative Services",
	"Service_Advertising and Marketing_Advertising Agencies and Media Buyers",
	"Service_Art Restoration",
	"Service_Audiovisual",
	"Service_Automation and Control Systems",
	"Service_Automotive",
	"Service_Automotive_Towing",
	"Service_Automotive_Motorcycle, Moped and Scooter Repair",
	"Service_Automotive_Maintenance and Repair",
	"Service_Automotive_Car Wash and Detail",
	"Service_Automotive_Car Appraisers",
	"Service_Automotive_Auto Transmission",
	"Service_Automotive_Auto Tires",
	"Service_Automotive_Auto Smog Check",
	"Service_Automotive_Auto Oil and Lube",
	"Service_Business and Strategy Consulting",
	"Service_Business Services",
	"Service_Business Services_Printing and Publishing",
	"Service_Cable",
	"Service_Chemicals and Gasses",
	"Service_Cleaning",
	"Service_Computers",
	"Service_Computers_Maintenance and Repair",
	"Service_Computers_Software Development",
	"Service_Construction",
	"Service_Construction_Specialty",
	"Service_Construction_Roofers",
	"Service_Construction_Painting",
	"Service_Construction_Masonry",
	"Service_Construction_Infrastructure",
	"Service_Construction_Heating, Ventilating and Air Conditioning",
	"Service_Construction_Electricians",
	"Service_Construction_Contractors",
	"Service_Construction_Carpet and Flooring",
	"Service_Construction_Carpenters",
	"Service_Credit Counseling and Bankruptcy Services",
	"Service_Dating and Escort",
	"Service_Employment Agencies",
	"Service_Engineering",
	"Service_Entertainment",
	"Service_Entertainment_Media",
	"Service_Events and Event Planning",
	"Service_Financial",
	"Service_Financial_Taxes",
	"Service_Financial_Student Aid and Grants",
	"Service_Financial_Stock Brokers",
	"Service_Financial_Loans and Mortgages",
	"Service_Financial_Holding and Investment Offices",
	"Service_Financial_Fund Raising",
	"Service_Financial_Financial Planning and Investments",
	"Service_Financial_Credit Reporting",
	"Service_Financial_Collections",
	"Service_Financial_Check Cashing",
	"Service_Financial_Business Brokers and Franchises",
	"Service_Financial_Banking and Finance",
	"Service_Financial_ATMs",
	"Service_Financial_Accounting and Bookkeeping",
	"Service_Food and Beverage",
	"Service_Food and Beverage_Distribution",
	"Service_Food and Beverage_Catering",
	"Service_Funeral Services",
	"Service_Geological",
	"Service_Home Improvement",
	"Service_Home Improvement_Upholstery",
	"Service_Home Improvement_Tree Service",
	"Service_Home Improvement_Swimming Pool Maintenance and Services",
	"Service_Home Improvement_Storage",
	"Service_Home Improvement_Roofers",
	"Service_Home Improvement_Pools and Spas",
	"Service_Home Improvement_Plumbing",
	"Service_Home Improvement_Pest Control",
	"Service_Home Improvement_Painting",
	"Service_Home Improvement_Movers",
	"Service_Home Improvement_Mobile Homes",
	"Service_Home Improvement_Lighting Fixtures",
	"Service_Home Improvement_Landscaping and Gardeners",
	"Service_Home Improvement_Kitchens",
	"Service_Home Improvement_Interior Design",
	"Service_Home Improvement_Housewares",
	"Service_Home Improvement_Home Inspection Services",
	"Service_Home Improvement_Home Appliances",
	"Service_Home Improvement_Heating, Ventilation and Air Conditioning",
	"Service_Home Improvement_Hardware and Services",
	"Service_Home Improvement_Fences, Fireplaces and Garage Doors",
	"Service_Home Improvement_Electricians",
	"Service_Home Improvement_Doors and Windows",
	"Service_Home Improvement_Contractors",
	"Service_Home Improvement_Carpet and Flooring",
	"Service_Home Improvement_Carpenters",
	"Service_Home Improvement_Architects",
	"Service_Household",
	"Service_Human Resources",
	"Service_Immigration",
	"Service_Import and Export",
	"Service_Industrial Machinery and Vehicles",
	"Service_Insurance",
	"Service_Internet Services",
	"Service_Leather",
	"Service_Legal",
	"Service_Logging and Sawmills",
	"Service_Machine Shops",
	"Service_Management",
	"Service_Manufacturing",
	"Service_Manufacturing_Apparel and Fabric Products",
	"Service_Manufacturing_Chemicals and Gasses",
	"Service_Manufacturing_Computers and Office Machines",
	"Service_Manufacturing_Electrical Equipment and Components",
	"Service_Manufacturing_Food and Beverage",
	"Service_Manufacturing_Furniture and Fixtures",
	"Service_Manufacturing_Glass Products",
	"Service_Manufacturing_Industrial Machinery and Equipment",
	"Service_Manufacturing_Leather Goods",
	"Service_Manufacturing_Metal Products",
	"Service_Manufacturing_Nonmetallic Mineral Products",
	"Service_Manufacturing_Paper Products",
	"Service_Manufacturing_Petroleum",
	"Service_Manufacturing_Plastic Products",
	"Service_Manufacturing_Rubber Products",
	"Service_Manufacturing_Service Instruments",
	"Service_Manufacturing_Textiles",
	"Service_Manufacturing_Tobacco",
	"Service_Manufacturing_Transportation Equipment",
	"Service_Manufacturing_Wood Products",
	"Service_Media Production",
	"Service_Metals",
	"Service_Mining",
	"Service_Mining_Coal",
	"Service_Mining_Metal",
	"Service_Mining_Non-Metallic Minerals",
	"Service_News Reporting",
	"Service_Oil and Gas",
	"Service_Packaging",
	"Service_Paper",
	"Service_Personal Care",
	"Service_Personal Care_Tattooing",
	"Service_Personal Care_Tanning Salons",
	"Service_Personal Care_Spas",
	"Service_Personal Care_Skin Care",
	"Service_Personal Care_Piercing",
	"Service_Personal Care_Massage Clinics and Therapists",
	"Service_Personal Care_Manicures and Pedicures",
	"Service_Personal Care_Laundry and Garment Services",
	"Service_Personal Care_Hair Salons and Barbers",
	"Service_Personal Care_Hair Removal",
	"Service_Petroleum",
	"Service_Photography",
	"Service_Plastics",
	"Service_Rail",
	"Service_Real Estate",
	"Service_Real Estate_Real Estate Development and Title Companies",
	"Service_Real Estate_Real Estate Appraiser",
	"Service_Real Estate_Real Estate Agents",
	"Service_Real Estate_Property Management",
	"Service_Real Estate_Corporate Housing",
	"Service_Real Estate_Commercial Real Estate",
	"Service_Real Estate_Building and Land Surveyors",
	"Service_Real Estate_Boarding Houses",
	"Service_Real Estate_Apartments, Condos and Houses",
	"Service_Real Estate_Rent",
	"Service_Refrigeration and Ice",
	"Service_Renewable Energy",
	"Service_Repair Services",
	"Service_Research",
	"Service_Rubber",
	"Service_Scientific",
	"Service_Security and Safety",
	"Service_Shipping and Freight",
	"Service_Software Development",
	"Service_Storage",
	"Service_Subscription",
	"Service_Tailors",
	"Service_Telecommunication Services",
	"Service_Textiles",
	"Service_Tourist Information and Services",
	"Service_Transportation",
	"Service_Travel Agents and Tour Operators",
	"Service_Utilities",
	"Service_Utilities_Water",
	"Service_Utilities_Sanitary and Waste Management",
	"Service_Utilities_Heating, Ventilating, and Air Conditioning",
	"Service_Utilities_Gas",
	"Service_Utilities_Electric",
	"Service_Veterinarians",
	"Service_Water and Waste Management",
	"Service_Web Design and Development",
	"Service_Welding",
	"Service_Agriculture and Forestry",
	"Service_Agriculture and Forestry_Crop Production",
	"Service_Agriculture and Forestry_Forestry",
	"Service_Agriculture and Forestry_Livestock and Animals",
	"Service_Agriculture and Forestry_Services",
	"Service_Art and Graphic Design",
	"Shops",
	"Shops_Adult",
	"Shops_Antiques",
	"Shops_Arts and Crafts",
	"Shops_Auctions",
	"Shops_Automotive",
	"Shops_Automotive_Used Car Dealers",
	"Shops_Automotive_Salvage Yards",
	"Shops_Automotive_RVs and Motor Homes",
	"Shops_Automotive_Motorcycles, Mopeds and Scooters",
	"Shops_Automotive_Classic and Antique Car",
	"Shops_Automotive_Car Parts and Accessories",
	"Shops_Automotive_Car Dealers and Leasing",
	"Shops_Beauty Products",
	"Shops_Bicycles",
	"Shops_Boat Dealers",
	"Shops_Bookstores",
	"Shops_Cards and Stationery",
	"Shops_Children",
	"Shops_Clothing and Accessories",
	"Shops_Clothing and Accessories_Women's Store",
	"Shops_Clothing and Accessories_Swimwear",
	"Shops_Clothing and Accessories_Shoe Store",
	"Shops_Clothing and Accessories_Men's Store",
	"Shops_Clothing and Accessories_Lingerie Store",
	"Shops_Clothing and Accessories_Kids' Store",
	"Shops_Clothing and Accessories_Boutique",
	"Shops_Clothing and Accessories_Accessories Store",
	"Shops_Computers and Electronics",
	"Shops_Computers and Electronics_Video Games",
	"Shops_Computers and Electronics_Mobile Phones",
	"Shops_Computers and Electronics_Cameras",
	"Shops_Construction Supplies",
	"Shops_Convenience Stores",
	"Shops_Costumes",
	"Shops_Dance and Music",
	"Shops_Department Stores",
	"Shops_Digital Purchase",
	"Shops_Discount Stores",
	"Shops_Electrical Equipment",
	"Shops_Equipment Rental",
	"Shops_Flea Markets",
	"Shops_Florists",
	"Shops_Food and Beverage Store",
	"Shops_Food and Beverage Store_Specialty",
	"Shops_Food and Beverage Store_Health Food",
	"Shops_Food and Beverage Store_Farmers Markets",
	"Shops_Food and Beverage Store_Beer, Wine and Spirits",
	"Shops_Fuel Dealer",
	"Shops_Furniture and Home Decor",
	"Shops_Gift and Novelty",
	"Shops_Glasses and Optometrist",
	"Shops_Hardware Store",
	"Shops_Hobby and Collectibles",
	"Shops_Industrial Supplies",
	"Shops_Jewelry and Watches",
	"Shops_Luggage",
	"Shops_Marine Supplies",
	"Shops_Music, Video and DVD",
	"Shops_Musical Instruments",
	"Shops_Newsstands",
	"Shops_Office Supplies",
	"Shops_Outlet",
	"Shops_Outlet_Women's Store",
	"Shops_Outlet_Swimwear",
	"Shops_Outlet_Shoe Store",
	"Shops_Outlet_Men's Store",
	"Shops_Outlet_Lingerie Store",
	"Shops_Outlet_Kids' Store",
	"Shops_Outlet_Boutique",
	"Shops_Outlet_Accessories Store",
	"Shops_Pawn Shops",
	"Shops_Pets",
	"Shops_Pharmacies",
	"Shops_Photos and Frames",
	"Shops_Shopping Centers and Malls",
	"Shops_Sporting Goods",
	"Shops_Supermarkets and Groceries",
	"Shops_Tobacco",
	"Shops_Toys",
	"Shops_Vintage and Thrift",
	"Shops_Warehouses and Wholesale Stores",
	"Shops_Wedding and Bridal",
	"Shops_Wholesale",
	"Shops_Lawn and Garden",
	"Tax",
	"Tax_Refund",
	"Tax_Payment",
	"Transfer",
	"Transfer_Internal Account Transfer",
	"Transfer_ACH",
	"Transfer_Billpay",
	"Transfer_Check",
	"Transfer_Credit",
	"Transfer_Debit",
	"Transfer_Deposit",
	"Transfer_Deposit_Check",
	"Transfer_Deposit_ATM",
	"Transfer_Keep the Change Savings Program",
	"Transfer_Payroll",
	"Transfer_Payroll_Benefits",
	"Transfer_Third Party",
	"Transfer_Third Party_Venmo",
	"Transfer_Third Party_Square Cash",
	"Transfer_Third Party_Square",
	"Transfer_Third Party_PayPal",
	"Transfer_Third Party_Dwolla",
	"Transfer_Third Party_Coinbase",
	"Transfer_Third Party_Chase QuickPay",
	"Transfer_Third Party_Acorns",
	"Transfer_Third Party_Digit",
	"Transfer_Third Party_Betterment",
	"Transfer_Third Party_Plaid",
	"Transfer_Wire",
	"Transfer_Withdrawal",
	"Transfer_Withdrawal_Check",
	"Transfer_Withdrawal_ATM",
	"Transfer_Save As You Go",
	"Travel",
	"Travel_Airlines and Aviation Services",
	"Travel_Airports",
	"Travel_Boat",
	"Travel_Bus Stations",
	"Travel_Car and Truck Rentals",
	"Travel_Car Service",
	"Travel_Car Service_Ride Share",
	"Travel_Charter Buses",
	"Travel_Cruises",
	"Travel_Gas Stations",
	"Travel_Heliports",
	"Travel_Limos and Chauffeurs",
	"Travel_Lodging",
	"Travel_Lodging_Resorts",
	"Travel_Lodging_Lodges and Vacation Rentals",
	"Travel_Lodging_Hotels and Motels",
	"Travel_Lodging_Hostels",
	"Travel_Lodging_Cottages and Cabins",
	"Travel_Lodging_Bed and Breakfasts",
	"Travel_Parking",
	"Travel_Public Transportation Services",
	"Travel_Rail",
	"Travel_Taxi",
	"Travel_Tolls and Fees",
	"Travel_Transportation Centers"
];
module.exports = categories;
