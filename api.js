'use strict';

const { Transaction } = require('./transaction');
const categories = require('./categories');

function getIndexLocals() {
	let mappedCategories = getCategoriesOptions();
	let years = getYearsOptions();
	let months = getMonthsOptions();
	let days = getDaysOptions();
	let daysOfWeek = getDaysOfWeekOptions();

	return {
		years,
		months,
		days,
		daysOfWeek,
		categories: mappedCategories
	};
}

function getCategoriesOptions() {
	let cats = [{ id: '', name: 'All' }];
	categories.forEach(category => cats.push({
		id: category,
		name: category.split('_').join(' > ')
	}));

	return cats;
}

function getYearsOptions() {
	let years = [{ id: '', name: 'All' }];

	for (let i = 0; i < 18; i++) {
		let year = 2000 + i;
		years.push({
			id: year,
			name: year
		});
	}

	return years;
}

function getMonthsOptions() {
	let months = [
		{ id: '', name: 'All' },
		{ id: 1, name: 'January' },
		{ id: 2, name: 'February' },
		{ id: 3, name: 'March' },
		{ id: 4, name: 'April' },
		{ id: 5, name: 'May' },
		{ id: 6, name: 'June' },
		{ id: 7, name: 'July' },
		{ id: 8, name: 'August' },
		{ id: 9, name: 'September' },
		{ id: 10, name: 'October' },
		{ id: 11, name: 'November' },
		{ id: 12, name: 'December' }
	];

	return months;
}

function getDaysOptions() {
	let days = [{ id: '', name: 'All' }];

	for (let day = 1; day <= 31; day++) {
		days.push({
			id: day,
			name: day
		});
	}

	return days;
}

function getDaysOfWeekOptions() {
	let daysOfWeek = [
		{ id: '', name: 'All' },
		{ id: 1, name: 'Sunday' },
		{ id: 2, name: 'Monday' },
		{ id: 3, name: 'Tuesday' },
		{ id: 4, name: 'Wednesday' },
		{ id: 5, name: 'Thursday' },
		{ id: 6, name: 'Friday' },
		{ id: 7, name: 'Saturday' },
	];

	return daysOfWeek;
}

async function getTransactions(query) {
	let q = {};
	let addFields = {};
	let fieldsToKeep = {
		location: true,
		accountId: true,
		category: true,
		amount: true,
		date: true
	};
	let {
		minAmount,
		maxAmount,
		category,
		day,
		month,
		year,
		dayOfWeek,

		sort = '-date',
		limit = 20
	} = query;

	// Mongo doesn't cast in aggregations so we do it ourselves
	// TODO: What to do with non-number arguments
	if (isNotEmptyString(minAmount) && isNotEmptyString(maxAmount)) {
		if (minAmount > maxAmount) [minAmount, maxAmount] = [maxAmount, minAmount];
		q.amount = { $gte: Number(minAmount), $lte: Number(maxAmount) };
	} else if (isNotEmptyString(minAmount) || isNotEmptyString(maxAmount)) {
		q.amount = Number(minAmount || maxAmount);
	}

	if (category) {
		// Not using buildQuery to avoid creating lots of RegExp objects
		let regex = `^${category}`;
		if (Array.isArray(category)) {
			regex = category.map(c => `^${c}`).join('|');
		}
		q.category = { $regex: regex };
	}

	if (day) {
		addFields.day = { $dayOfMonth: '$date' };
		q.day = buildQuery(day, Number);
	}

	if (month) {
		addFields.month = { $month: '$date' };
		q.month = buildQuery(month, Number);
	}

	if (year) {
		addFields.year = { $year: '$date' };
		q.year = buildQuery(year, Number);
	}

	if (dayOfWeek) {
		addFields.dayOfWeek = { $dayOfWeek: '$date' };
		q.dayOfWeek = buildQuery(dayOfWeek, Number);
	}

	let t = Transaction.aggregate();
	let statsPipeline = [
		{
			$match: {
				amount: { $exists: true }
			}
		},
		{
			$group: {
				_id: null,
				averageAmount: { $avg: '$amount' },
				minAmount: { $min: '$amount' },
				maxAmount: { $max: '$amount' }
			}
		}
	];
	let cleanTransactionsPipeline = [{
		$project: fieldsToKeep
	}];

	if (Object.keys(addFields).length) t.addFields(addFields);

	t.match(q).sort(sort).limit(limit);
	return t.facet({
		stats: statsPipeline,
		transactions: cleanTransactionsPipeline
	});
}

function isNotEmptyString(val) {
	return val !== undefined && val !== '';
}

function buildQuery(valOrArray, mapFunc) {
	if (Array.isArray(valOrArray)) {
		return { $in: mapFunc ? valOrArray.map(mapFunc) : valOrArray };
	}

	return mapFunc ? mapFunc(valOrArray) : valOrArray;
}

module.exports = {
	getIndexLocals,
	getTransactions
};
