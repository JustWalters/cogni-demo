'use strict';

const mongoose = require('mongoose');
const categories = require('./categories');

const transactionSchema = mongoose.Schema({
	location: {
		address: String,
		city: String,
		state: String,
		zip: Number,
		country: {
			type: String,
			default: 'US'
		}
	},
	accountId: String,
	category: {
		type: String,
		enum: categories
	},
	amount: Number,
	date: Date
});

const Transaction = mongoose.model('Transaction', transactionSchema);

function generateRandomTransactions(num = 10) {
	let location, accountId, category, amount, date, transactions = [];

	for (let i = 0; i < num; i++) {
		location = getRandomLocation();
		accountId = getRandomId();
		category = getRandomCategories();
		amount = getRandomAmount();
		date = getRandomDate();

		transactions.push({
			location,
			accountId,
			category,
			amount,
			date
		});
	}

	return Transaction.create(transactions);
}

function generateRandomTransaction() {
	let location = getRandomLocation();
	let accountId = getRandomId();
	let category = getRandomCategories();
	let amount = getRandomAmount();
	let date = getRandomDate();

	let transaction = new Transaction({
		location,
		accountId,
		category,
		amount,
		date
	});
	return transaction;
}

function getRandomLocation() {
	// This will return some invalid/non-existent addresses. Very Manhattan-focused
	let streets = ['Broadway', '1st Ave', '2nd Ave', 'Lexington Ave', 'Amsterdam Ave'];
	return {
		address: `${getRandomNumber(1000)} ${streets[getRandomNumber(streets.length)]}`,
		city: 'New York',
		state: 'NY',
		zip: 10000 + getRandomNumber(287),
		country: 'US'
	};
}

function getRandomId() {
	// These are account ids, so we want repeats, but not too many
	return `${getRandomNumber(100)}`;
}

function getRandomCategories() {
	return categories[getRandomNumber(categories.length)];
}

function getRandomAmount() {
	// Want most values to be on the low-end, but imprecision here is okay
	let weighting = Math.pow(Math.random(), 3);
	return roundToCents(weighting * 500);
}

function getRandomDate() {
	// Only get years from 2000-2017
	let year = 2000 + getRandomNumber(18);
	let month = getRandomNumber(12);
	let day = getRandomNumber(31);
	let hours = getRandomNumber(24);
	let minutes = getRandomNumber(60);
	return new Date(year, month, day, hours, minutes);
}

function getRandomNumber(noGreaterThan = 1) {
	return Math.floor(Math.random() * noGreaterThan);
}

function roundToCents(num) {
	let factor = 100;
	return Math.round(num * factor) / factor;
}

module.exports = {
	Transaction,
	generateRandomTransaction,
	generateRandomTransactions
};
