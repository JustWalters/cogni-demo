'use strict';

const mongoose = require('mongoose');
const { Transaction, generateRandomTransactions } = require('./transaction');
const { startServer } = require('./server');

mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost/cogni-demo')
	.then(init)
	.catch(onError('Connection Error:'));

function onError(msg) {
	return function(err) {
		console.error(`${msg} ${err}`);
		mongoose.disconnect();
	};
}

function init() {
	startServer();
	Transaction.count({}, function(err, count) {
		if (err) {
			return onError('Count error:');
		}

		if (count) {
			console.log(`${count} existing transactions`);
			return;
		}

		generateRandomTransactions(1000)
			.then(() => {
				console.log('Success');
			})
			.catch(onError('Generating transactions:'));
	});
}
