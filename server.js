'use strict';

const express = require('express');
const { getTransactions, getIndexLocals } = require('./api');

const app = express();
app.set('view engine', 'pug');

app.get('/transactions', function(req, res) {
	return getTransactions(req.query)
		.then(transactionsAndStats => {
			let transactions = transactionsAndStats[0].transactions;
			let stats = transactionsAndStats[0].stats[0];
			let headers = ['Account Id', 'Category', 'Amount', 'Date', 'Address'];
			let displayProps = ['displayAccountId', 'displayCategory', 'displayAmount', 'displayDate', 'displayAddress'];

			transactions.forEach(function(t) {
				t.displayAccountId = t.accountId;
				t.displayCategory = t.category.split('_').join(' > ');
				t.displayAmount = t.amount;
				t.displayDate = t.date.toLocaleString();
				t.displayAddress = `${t.location.address} ${t.location.city}, ${t.location.state} ${t.location.zip}`;
			});
			res.render('results', { headers, displayProps, transactions, stats });
		})
		.catch(err => {
			res.status(500).send(err);
		});
});

app.locals = getIndexLocals();
app.get('/*', function(req, res) {
	return res.render('index');
});

function startServer() {
	app.listen(3000, function() {
		console.log('Example app listening on port 3000!');
	});
}

module.exports = {
	startServer
};
